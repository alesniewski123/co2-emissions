# CO2 emissions
The goal of the following study is to create a prediction model for the CO2 emissions of new cars. The stady uses "Fuel consumption ratings" prepared by the Canadian government. For predicting CO2 emissions I used 3 different machine learning models: Multiple Linear Regression, Decision tree, and Random Forest.

The topic of predicting CO2 emissions of new cars can be especially interesting regarding the Volkswagen emissions scandal. Machine learning models can be used to predict emission rates which can be compared with manufacturers' declarations.

